module gitlab.com/yakshaving.art/durrs/tmpldurr

go 1.16

require (
	github.com/sirupsen/logrus v1.6.0
	github.com/stretchr/testify v1.6.1
	github.com/xanzy/go-gitlab v0.37.0
	gopkg.in/yaml.v2 v2.3.0
)
