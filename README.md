# tmpldurr

Because sometimes a little bit of DRY isn't all that poisonous. Only sometimes.

## Convince me

Gitlab issue templates are great. They ensure consistency over issues being
reported, and enable self-servicing for users. This is all hunky-dory as long as
you work on a single project. Things get annoying really fast as soon as you
venture beyond that. [There isn't yet an official way][group-templates-issue] in
Gitlab to keep templates consistent across a number of projects within the same
group, let alone across the whole instance.

So here we are, trying to make your (and your users') life easier by saving you
the burden of manually keeping all those templates in sync.

## Overview

What `tmpldurr` does is to simply push the templates stored in a central SSOT to
a number of target projects. Whenever a template changes, `tmpldurr` will detect
it and push the new version to all the configured targets.

`tmpldurr` is designed to run in a CI pipeline as a Docker image. The suggested
implementation is to create your own repository with templates stored in the
`/templates` directory. In the same repository, also add a configuration file
that stores the template/project mappings. More on that
[later](#configuration-file). Depending on how your organisation scales, you can
choose to use a single, instance-wise `tmpldurr` deployment or more.

## Caveats

* For the time being, the user bound to the token must have enough privileges to
  directly push to master. In the future, we'll add a flag to create a merge
  request instead.
* `tmpldurr` does not remove issue templates from projects. You have to do the
  cleanup work yourself.
* `tmpldurr` pushes templates using their file name, not the full path of the
  templates directory. So if you try and push two templates with the same file
  name then you'll end up in a race condition, even if they are stored in
  different subdirectories.

## Installation

This project builds a Docker image and publishes it to the gitlab.com registry.
You can then set up your own CI pipeline that injects the configuration file in
the upstream `tmpldurr` image and then runs it. Simple as that.

## Usage

`tmpldurr <args> <configuration file>`

### Required Environment Variables

- **GITLAB_TOKEN** the token to use when contacting the GitLab instance API.
- **GITLAB_BASEURL** the GitLab instance URL to talk to.

### Arguments

#### -autodevops

Commit directly to master instead of opening a merge request (default: true).

#### -config

The configuration file.

#### -debug

Enable debug mode.

#### -dry-run

Show the actions without actually doing anything.

#### -templates-dir

The directory that contains the templates.

#### -version

Prints the version and exits.

## Configuration file

The format is the following:

```yaml
template-file-name-1:
  - project-id-1
  - project-id-3
template-file-name-2:
  - project-id-2
  - project-id-4
  - project-id-5
template-file-name-3:
  - project-id-6
  - project-id-7
  - project-id-8
```

### Sample configuration file

```yaml
new_feature_request.md:
  - 18111121
  - 17484737
  - 16271131
something_is_broken.md:
  - 14442515
  - 17484737
  - 16271131
```

[group-templates-issue]: https://gitlab.com/gitlab-org/gitlab/-/issues/7749
