package main

import (
	log "github.com/sirupsen/logrus"

	"gitlab.com/yakshaving.art/durrs/tmpldurr/internal/config"
	"gitlab.com/yakshaving.art/durrs/tmpldurr/internal/tmpldurr"
)

func main() {
	config := config.ParseArgs()
	log.SetLevel(log.InfoLevel)
	if config.Debug {
		log.SetLevel(log.DebugLevel)
	}
	if config.DryRun {
		log.Infoln("Dry-run mode is enabled. I won't write anything.")
	}

	s := tmpldurr.New(config)
	for templateName := range config.TemplatesMap {
		if err := s.SyncTemplate(templateName); err != nil {
			log.Error(err)
		}
	}
}
