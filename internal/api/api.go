package api

import (
	"crypto/sha256"
	"fmt"
	"path/filepath"

	log "github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/yakshaving.art/durrs/tmpldurr/internal/config"
)

const (
	createCommitMessage     = "Creating issue template"
	gitlabIssueTemplatePath = ".gitlab/issue_templates"
	updateCommitMessage     = "Updating issue template"
)

// GitLabClient is a GitLab API client with some project options
type GitLabClient struct {
	Client *gitlab.Client
	DryRun bool
}

// NewClient creates a new API client to a GitLab endpoint
func NewClient(args *config.Config) GitLabClient {
	client, err := gitlab.NewClient(args.GitlabToken, gitlab.WithBaseURL(args.GitlabBaseURL))
	if err != nil {
		log.Fatalf("failed to create gitlab client: %q", err)
	}
	return GitLabClient{
		Client: client,
		DryRun: args.DryRun,
	}
}

// PushIssueTemplate pushes the issue template to the target repo in .gitlab/issue_templates/
func (g GitLabClient) PushIssueTemplate(ProjectID int, TemplateName string, TemplateBody string) error {
	templateFile := filepath.Base(TemplateName)
	filePath := fmt.Sprintf("%s/%s", gitlabIssueTemplatePath, templateFile)

	if _, _, err := g.Client.Projects.GetProject(ProjectID, &gitlab.GetProjectOptions{}); err != nil {
		return err
	}

	// TODO: Implement non-AutoDevOps mode
	getFileOpts := &gitlab.GetFileOptions{Ref: gitlab.String("master")}
	f, _, err := g.Client.RepositoryFiles.GetFile(ProjectID, filePath, getFileOpts)
	if err != nil {
		// ASSUMPTION ALERT!
		// If a project is found but not the file, then we need to create the template
		return g.CreateTemplate(ProjectID, templateFile, TemplateBody, filePath)
	}

	if f.SHA256 == shaFile(TemplateBody) {
		log.Infof("No changes detected for template %s on project %d", templateFile, ProjectID)
		return nil
	}
	return g.UpdateTemplate(ProjectID, templateFile, TemplateBody, filePath)
}

// UpdateTemplate creates an issue template by pushing a new file to master
func (g GitLabClient) UpdateTemplate(ProjectID int, TemplateName string, TemplateBody string, filePath string) error {
	commitMessage := fmt.Sprintf("%s %s", updateCommitMessage, TemplateName)
	updateFileOpts := &gitlab.UpdateFileOptions{
		Branch:        gitlab.String("master"),
		Content:       gitlab.String(TemplateBody),
		CommitMessage: gitlab.String(commitMessage),
	}
	if g.DryRun {
		log.Infof("I would have updated this file :\nProject ID: %d\nFile: %s\n", ProjectID, filePath)
		return nil
	}
	_, _, err := g.Client.RepositoryFiles.UpdateFile(ProjectID, filePath, updateFileOpts)
	if err != nil {
		return err
	}
	log.Infof("Successfully updated issue template %s on project %d", TemplateName, ProjectID)
	return nil
}

// CreateTemplate creates an issue template by pushing a new file to master
func (g GitLabClient) CreateTemplate(ProjectID int, TemplateName string, TemplateBody string, filePath string) error {
	commitMessage := fmt.Sprintf("%s %s", createCommitMessage, TemplateName)
	createFileOpts := &gitlab.CreateFileOptions{
		Branch:        gitlab.String("master"),
		Content:       gitlab.String(TemplateBody),
		CommitMessage: gitlab.String(commitMessage),
	}
	if g.DryRun {
		log.Infof("I would have created a new file:\nProject ID: %d\nFile: %s\n", ProjectID, filePath)
		return nil
	}
	_, _, err := g.Client.RepositoryFiles.CreateFile(ProjectID, filePath, createFileOpts)
	if err != nil {
		return err
	}
	log.Infof("Successfully created issue template %s on project %d", TemplateName, ProjectID)
	return nil
}

func shaFile(body string) string {
	sha := sha256.New()
	sha.Write([]byte(body))
	return fmt.Sprintf("%x", sha.Sum(nil))
}
