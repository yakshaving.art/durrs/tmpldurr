package config_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/yakshaving.art/durrs/tmpldurr/internal/config"
)

func TestLoadConfig(t *testing.T) {
	tt := []struct {
		name   string
		config config.Config
		err    string
	}{
		{
			"valid configuration",
			config.Config{
				ConfigFile: "test-fixtures/valid-config.yaml",
			},
			"",
		},
		{
			"empty configuration",
			config.Config{
				ConfigFile: "test-fixtures/empty-config.yaml",
			},
			"",
		},
		{
			"no configuration file specified",
			config.Config{
				ConfigFile: "",
			},
			"configuration file not specified. Exiting",
		},
		{
			"configuration file not found",
			config.Config{
				ConfigFile: "test-fixtures/I-am-not-here.yaml",
			},
			"failed to read configuration file test-fixtures/I-am-not-here.yaml: open test-fixtures/I-am-not-here.yaml: no such file or directory",
		},
		{
			"invalid configuration",
			config.Config{
				ConfigFile: "test-fixtures/invalid-config.yaml",
			},
			"failed to parse configuration file test-fixtures/invalid-config.yaml: yaml: unmarshal errors:\n  line 3: cannot unmarshal !!map into []int",
		},
	}

	for _, tc := range tt {
		a := assert.New(t)
		t.Run(tc.name, func(t *testing.T) {
			err := tc.config.LoadConfig(tc.config.ConfigFile)
			if tc.err != "" {
				a.EqualError(err, tc.err)
			} else {
				a.NoError(err)
			}
		})
	}
}
