package config

import (
	"flag"
	"os"

	log "github.com/sirupsen/logrus"
)

// ParseArgs parses the command line arguments
func ParseArgs() *Config {
	args := &Config{}

	flag.BoolVar(&args.AutoDevOps, "autodevops", true, "push directly to master rather than creating a merge request")
	flag.BoolVar(&args.Debug, "debug", false, "enable debug level logging")
	flag.BoolVar(&args.DryRun, "dry-run", false, "don't apply anything, only look at what would happen")
	flag.BoolVar(&args.Version, "version", false, "show version and exit")
	flag.StringVar(&args.ConfigFile, "config", "config.yaml", "configuration file")
	flag.StringVar(&args.TemplatesDir, "templates-dir", "templates", "directory where the templates are stored")

	flag.Parse()

	args.GitlabToken = os.Getenv("GITLAB_TOKEN")
	args.GitlabBaseURL = os.Getenv("GITLAB_BASEURL")

	if args.GitlabToken == "" {
		log.Fatal("GITLAB_TOKEN is a required environment variable")
	}

	if args.GitlabBaseURL == "" {
		log.Fatal("GITLAB_BASEURL is a required environment variable")
	}

	if e := args.LoadConfig(args.ConfigFile); e != nil {
		log.Fatalf("error loading the configuration file: %s", e)
	}

	return args
}
