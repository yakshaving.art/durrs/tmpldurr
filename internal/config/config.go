package config

import (
	"fmt"
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

// Config is the configuration that can be used in this binary.
type Config struct {
	AutoDevOps    bool
	ConfigFile    string
	Debug         bool
	DryRun        bool
	GitlabBaseURL string
	GitlabToken   string
	TemplatesDir  string
	TemplatesMap  map[string][]int
	Version       bool
}

// LoadConfig validates and loads the configuration file in the Config object
func (c *Config) LoadConfig(filename string) error {
	if filename == "" {
		return fmt.Errorf("configuration file not specified. Exiting")
	}

	f, err := ioutil.ReadFile(filename)
	if err != nil {
		return fmt.Errorf("failed to read configuration file %s: %s", filename, err)
	}

	if err := yaml.UnmarshalStrict(f, &c.TemplatesMap); err != nil {
		return fmt.Errorf("failed to parse configuration file %s: %s", filename, err)
	}

	return nil
}
