package tmpldurr

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	log "github.com/sirupsen/logrus"
	"gitlab.com/yakshaving.art/durrs/tmpldurr/internal/api"
	"gitlab.com/yakshaving.art/durrs/tmpldurr/internal/config"
)

// Tmpldurr contains all that's needed to sync templates
type Tmpldurr struct {
	Config    *config.Config
	Gitlab    api.GitLabClient
	Templates map[string]string
}

// New creates an API client and prepares to work on the templates
func New(config *config.Config) Tmpldurr {
	g := api.NewClient(config)
	g.Client.BaseURL()
	templates, err := loadTemplates(config.TemplatesDir)
	if err != nil {
		log.Fatalf("failed to load templates: %s", err)
	}
	return Tmpldurr{
		Config:    config,
		Gitlab:    g,
		Templates: templates,
	}
}

// SyncTemplate updates or creates an issue template to a group of projects
func (td Tmpldurr) SyncTemplate(templateName string) error {
	templateBody, found := td.Templates[templateName]
	if !found {
		return fmt.Errorf("template %s not found in memory", templateName)
	}

	projects := td.Config.TemplatesMap[templateName]
	for _, projectID := range projects {
		err := td.Gitlab.PushIssueTemplate(projectID, templateName, templateBody)
		if err != nil {
			return err
		}
	}
	return nil
}

// loadTemplates fetches the available templates from the templates directory
func loadTemplates(templatesDir string) (map[string]string, error) {
	templates := make(map[string]string)
	err := filepath.Walk(templatesDir, func(path string, info os.FileInfo, err error) error {
		thisfile, err := os.Stat(path)
		if err != nil {
			return err
		}
		if thisfile.IsDir() {
			log.Debugf("%s is a directory. Ignoring...", path)
			return nil
		}

		relPath, err := filepath.Rel(templatesDir, path)
		if err != nil {
			return err
		}
		log.Debugf("loading template %s", relPath)
		contents, err := ioutil.ReadFile(path)
		if err != nil {
			return err
		}
		templates[relPath] = string(contents)
		return nil
	})
	if err != nil {
		return nil, err
	}
	log.Debugf("succesfully loaded %d templates in memory", len(templates))
	return templates, nil
}
